#include <regex>
#include <fstream>
#include <iostream>
#include <SFML/Graphics.hpp>

#include "layers.h"
#include "networks.h"

#define OUT_NEURONS 10
#define HIDDEN_NEURONS 100
#define INPUT_NEURONS 784
#define TRAIN_COUNT 60000
#define TEST_COUNT 10000
#define VISUALIZAITION false

std::vector<std::string> split(const std::string& input, const std::string& regex) {
    // passing -1 as the submatch index parameter performs splitting
    std::regex re(regex);
    std::sregex_token_iterator
        first{input.begin(), input.end(), re, -1},
        last;

    return {first, last};
}

void vecPtrinter(const std::vector<std::string>& vec) {
    for (const auto& item : vec) {
        std::cout << item << ", ";
    }
    std::cout << std::endl;
    std::cout << vec.size() << std::endl;
}

void train(const std::string& filepath, faai::Neural& network) {
    std::ifstream fin;
    fin.open(filepath);
    int count = 0;

    sf::RenderWindow window(sf::VideoMode(1000, 1000), "FAAI");

    std::vector<sf::CircleShape> shapes1;
    shapes1.resize(784);
    for (int i = 0; i < 784; i++) {
        shapes1[i].setRadius(1);
        shapes1[i].setPosition(0, i);
    }

    std::vector<sf::CircleShape> shapes2;
    shapes2.resize(100);
    for (int i = 0; i < 100; i++) {
        shapes2[i].setRadius(7);
        shapes2[i].setPosition(300, i * 10);
    }

    std::vector<sf::CircleShape> shapes3;
    shapes3.resize(10);
    for (int i = 0; i < 10; i++) {
        shapes3[i].setRadius(20);
        shapes3[i].setPosition(600, i * 50);
    }
    while (window.isOpen()) {
        for (int i = 0; i < 784; i++) {
            window.draw(shapes1[i]);
        }
        for (int i = 0; i < 100; i++) {
            window.draw(shapes2[i]);
        }
        for (int i = 0; i < 10; i++) {
            window.draw(shapes3[i]);
        }
        while (count < TRAIN_COUNT) {
            count++;
            if (count % 100 == 0) {
                sf::VertexArray lin(sf::LinesStrip, 2);
                for (int i = 0; i < 784 * 100; i++) {
                    int x = i / 100;
                    int y = i % 100;
                    float w = network.getWeight(0, x, y);
                    if (fabsf(w) < 0.6)
                        continue;
                    if (w > 0) {
                        lin[0].color = sf::Color::Red;
                        lin[1].color = sf::Color::Red;
                    }
                    else {
                        lin[0].color = sf::Color::Blue;
                        lin[1].color = sf::Color::Blue;
                    }
                    lin[0].position = sf::Vector2f(0, x);
                    lin[1].position = sf::Vector2f(300, 10 * (y) + 3);
                    window.draw(lin);
                }

                for (int i = 0; i < 100 * 10; i++) {
                    int x = i / 10;
                    int y = i % 10;
                    float w = network.getWeight(1, x, y);
                    if (fabsf(w) < 0.7)
                        continue;
                    if (w > 0) {
                        lin[0].color = sf::Color::Red;
                        lin[1].color = sf::Color::Red;
                    }
                    else {
                        lin[0].color = sf::Color::Blue;
                        lin[1].color = sf::Color::Blue;
                    }
                    lin[0].position = sf::Vector2f(300, 10 * (x) + 3);
                    lin[1].position = sf::Vector2f(600, 50 * (y) + 10);
                    window.draw(lin);
                }
                //std::this_thread::sleep_for(std::chrono::seconds(1));"
                window.display();
            }
            std::string line;
            std::getline(fin, line);
            if (fin.eof()) {
                break;
            }
            std::vector <std::string> data_vector = split(line, ",");
            if (data_vector.size() != 785) {
                break;
            }
            std::size_t answer = std::atoi(data_vector[0].c_str());
            data_vector.erase(data_vector.begin());
            prep::Matrix data_matrix(data_vector);
            prep::Matrix oo1_matrix(1, data_vector.size(), 0.01);

            data_matrix = (data_matrix * (1.0 / 255.0) * 0.99) + oo1_matrix;

            prep::Matrix target_matrix(1, OUT_NEURONS);
            for (std::size_t i = 0; i < OUT_NEURONS; i++) {
                target_matrix(0, i) = 0.01;
                if (i == answer) {
                    target_matrix(0, i) = 0.99;
                }
            }
            network.train(data_matrix.transp(), target_matrix.transp());
        }
    }
    fin.close();
}

void printDigit(const prep::Matrix& data_matrix) {
    for (std::size_t i = 0; i < 28; i++) {
        for (std::size_t j = 0; j < 28; j++) {
            if (data_matrix(0, i * 28 + j) > 0.02) {
                std::cout << "#";
            } else {
                std::cout << "_";
            }
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

void query(const std::string& filepath, faai::Neural& network) {
    std::ifstream fin;
    fin.open(filepath);
    int count_true_answer = 0;
    int count_all = 0;
    while (count_all < TEST_COUNT) {
        std::string line;
        std::getline(fin, line);
        if (fin.eof()) {
            break;
        }
        count_all++;
        std::vector<std::string> data_vector = split(line, ",");
        if (data_vector.size() != 785) {
            break;
        }
        std::size_t answer = std::atoi(data_vector[0].c_str());
        data_vector.erase(data_vector.begin());
        prep::Matrix data_matrix(data_vector);
        prep::Matrix oo1_matrix(1, data_vector.size(), 0.01);

        data_matrix = (data_matrix * (1.0 / 255.0) * 0.99) + oo1_matrix;
        prep::Matrix output_array = network.query(data_matrix.transp());

        prep::Matrix target_matrix(1, OUT_NEURONS);
        for (std::size_t i = 0; i < OUT_NEURONS; i++) {
            target_matrix(0, i) = 0.01;
            if (i == answer) {
                target_matrix(0, i) = 0.99;
            }
        }

        float max_arg = 0;
        std::size_t max_index = 0;
        for (std::size_t i = 0; i < OUT_NEURONS; i++) {
            if (max_arg < output_array(i, 0)) {
                max_arg = output_array(i, 0);
                max_index = i;
            }
        }
        if (max_index == answer) {
            count_true_answer++;
        } else {
            if (VISUALIZAITION) {
                std::cout << max_index << std::endl;
                printDigit(data_matrix);
            }
        }
    }
    std::cout << std::endl << "Процент правильных ответов: ";
    std::cout << 100 * static_cast<float>(count_true_answer) / static_cast<float>(count_all);
    std::cout << "%" << std::endl << std::endl;
    fin.close();
}

int main() {
    clock_t start = clock();

    faai::Neural network(784, 0.3);
    faai::Dense hidden_layer(100, faai::activation::sigmoid);
    faai::Dense output_layer(10, faai::activation::sigmoid);
    network.addLayer(hidden_layer);
    network.addLayer(output_layer);
    train("train.csv", network);
    query("train.csv", network);

    clock_t end = clock();
    float seconds = static_cast<float>(end - start) / CLOCKS_PER_SEC;
    std::cout << seconds << std::endl;
}

