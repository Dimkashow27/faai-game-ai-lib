#include "aibotsnake.h"

BOT::BOT(std::string name) :
// AI(std::size_t actions_count, std::map<int, float> objects, std::size_t input_count, std::string filename);
            ai(3, {{0, -0.5}, {1, -0.99}, {2, 0.99}, {3, 0.01}}, 25, name),
            name(name) {}

std::vector<float> BOT::step(const prep::Matrix& matrix) {
    std::vector<int> env;
    for (std::size_t i = 0; i < matrix.getRows(); i++) {
        for (std::size_t j = 0; j < matrix.getCols(); j++) {
            env.push_back(static_cast<int>(matrix(i, j)));
        }
    }
    return ai.step(env);
}

void BOT::registerResult(const prep::Matrix& env, float result) {
    std::vector<int> env_tovec;
    for (std::size_t i = 0; i < env.getRows(); i++) {
        for (std::size_t j = 0; j < env.getCols(); j++) {
            env_tovec.push_back(static_cast<int>(env(i, j)));
        }
    }
    ai.registerResult(env_tovec, result);
}

void BOT::trainAll() {
    ai.trainAll();
}

void BOT::trainNow(float result) {
    ai.trainNow(result);
}

void BOT::save() const {
    ai.save(name);
}

