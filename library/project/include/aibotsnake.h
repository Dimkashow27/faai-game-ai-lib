#pragma once

#include <map>
#include <vector>
#include <string>
#include "classai.h"

class BOT {
 private:
    AI::AI ai;
    std::string name;

 public:
    explicit BOT(std::string name);

    std::vector<float> step(const prep::Matrix& matrix);
    void registerResult(const prep::Matrix& env, float result);
    void trainAll();
    void trainNow(float result);
    void save() const;
};

