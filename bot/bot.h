#pragma once

#include <vector>
#include <string>

class BOT {
 private:
    // AI::AI ai;
    std::string name;

 public:
    explicit BOT(std::string name);

    int step(const std::vector<int>& env);
    void registerResult(const std::vector<int>& env, float award);
    void trainAll();
    void trainNow(float award);
    void save() const;
};

